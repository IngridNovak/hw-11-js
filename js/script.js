"use strict";
let div = document.querySelectorAll('[data-form = "icon"]');
div.forEach(item => {
    item.addEventListener('click', changeIcon);

    function changeIcon(e) {
        if (e.target.classList.contains("fa-eye")) {
            e.target.classList.remove("fa-eye");
            e.target.classList.add("fa-eye-slash");
        } else {
            e.target.classList.remove("fa-eye-slash");
            e.target.classList.add("fa-eye");
        }


        let inputs = document.querySelectorAll('.inp');


        inputs.forEach(input => {
            if (e.target.getAttribute('data-icon') === input.getAttribute('data-type')) {

                if (input.type === "password") {
                    input.type = 'text';
                } else {
                    input.type = 'password';
                }

            }
        })
    }
})


let button = document.getElementById('btn');
button.addEventListener('click', checkPassword);

function checkPassword() {
    let inputOne = document.getElementById('passOne');
    let inputTwo = document.getElementById('passTwo');
    let inputOneValue = inputOne.value.trim();
    let inputTwoValue = inputTwo.value.trim();
    if (inputOneValue === '' || inputTwoValue === '') {
        let message = document.getElementById('message');
        message.innerText = 'Введіть значення';
        message.style.color = 'red';
    } else if (inputOneValue !== inputTwoValue) {
        let message = document.getElementById('message');
        message.innerText = 'Потрібно ввести однакові значення'
        message.style.color = 'red';
    } else {
        let welcome = 'You are welcome';

        alert(welcome);
    }

}

